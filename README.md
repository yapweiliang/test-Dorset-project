thank you for making it thus far.

please try editing the pages in the Wiki to see if this is something that would 'work' for you.

simple formatting can be done.  For example to underline, you simply write the title, and below that you write a series of dashes.
For bullet points, put a dash or asterisk followed by a space, then your text.

Make a double space between paragraphs.

but don't worry about formatting, etc.  I can tidy that up.  It is the content that matters.

all changes are recorded.

this particular 'project' is public.  the intention if agreed would be to set up a private project.  nevertheless
anything you write here must be 'safe' for public viewing, and not cause offense to colleagues, if not, do not write it here.



thanks,
Wei Liang